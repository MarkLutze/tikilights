#ifndef Ir_commands_h

#define Ir_commands_h

#define POWER_ON     0x3F
#define POWER_OFF    0xBF
#define BRIGHT_DOWN  0x7F
#define BRIGHT_UP    0xFF
#define RED_0        0xDF
#define RED_1        0xEF
#define RED_2        0xCF
#define RED_3        0xF7
#define RED_4        0xD7
#define GREEN_0      0x5F
#define GREEN_1      0x6F
#define GREEN_2      0x4F
#define GREEN_3      0x77
#define GREEN_4      0x57
#define BLUE_0       0x9F
#define BLUE_1       0xAF
#define BLUE_2       0x8F
#define BLUE_3       0xB7
#define BLUE_4       0x97
#define WHITE        0x1F
#define FLASH        0x2F
#define STROBE       0x0F
#define FADE         0x37
#define SMOOTH       0x17


#define RED_0_C        CRGB(255, 0, 0)
#define RED_1_C        CRGB(199, 10, 14)
#define RED_2_C        CRGB(199, 39, 17)
#define RED_3_C        CRGB(220, 123, 17)
#define RED_4_C        CRGB(225, 190, 0)

#define GREEN_0_C      CRGB(0, 255, 0)
#define GREEN_1_C      CRGB(14, 169, 59)
#define GREEN_2_C      CRGB(2, 65, 112)
#define GREEN_3_C      CRGB(13, 68, 141)
#define GREEN_4_C      CRGB(0, 32, 171)

#define BLUE_0_C       CRGB(0, 0, 255)
#define BLUE_1_C       CRGB(15, 7, 150)
#define BLUE_2_C       CRGB(193, 10, 193)
#define BLUE_3_C       CRGB(165, 28, 143)
#define BLUE_4_C       CRGB(193, 0, 68)

#define WHITE_C        CRGB::White





#define SHOW_COUNT  9

#endif
