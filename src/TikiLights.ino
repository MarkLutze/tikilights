#include "FastLED.h"

// How many leds in your strip?
#define NUM_LEDS 50
#define DATA_PIN 9
#define FRAMES_PER_SECOND  120
#define COLOR_ORDER RBG

// Settings for Fire2012
bool gReverseDirection = false;


#include "ir_commands.h"
#include <IRremote.h>

#define TRUE -1
#define FALSE 0

#define IR_PIN       10    // Pin to read IR commands
int ir_code;
IRrecv irrecv(IR_PIN);
decode_results ir_results; // Ir data buffer

int power_state = 0;
int brightness = 96;

CRGB base_color =  CRGB::White;

CRGB leds[NUM_LEDS];

uint8_t solid_color = TRUE ;
uint8_t addglitter = FALSE;

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

void setup() {

  Serial.begin(9600);
  delay(2000); while (!Serial); //delay for Leonardo
  irrecv.enableIRIn(); // Start the receiver

  Serial.println("initializing");

  FastLED.addLeds<WS2811, DATA_PIN, RGB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);;
  fill_solid( leds, NUM_LEDS, base_color);
  FastLED.setBrightness(brightness);


}

typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = {juggle, rainbow, rainbowWithGlitter, confetti, sinelon, bpm, Fire2012 };

void loop() {


 if (irrecv.decode(&ir_results)) {
    Serial.println(ir_results.value, HEX);

    if (ir_results.value == 0xFFFFFFFF) {
      ir_code = 0;
    } else {
      ir_code = 0x00FF & ir_results.value;
      processIR(ir_code);
    }

   irrecv.resume(); // Receive the next value
  } // got an IR code

  if (power_state) {
    if (solid_color){
      fill_solid( leds, NUM_LEDS, base_color);
      FastLED.setBrightness(brightness);
      if (addglitter) {
        addGlitter(30);
      }
    } else {
      gPatterns[gCurrentPatternNumber]();
    }
  } else {
    fill_solid( leds, NUM_LEDS, CRGB::Black);
  }

  if (IR_idle()) {
    FastLED.show();
  } else {
    Serial.println("skipped FastLED show()");
  }

  //standard delay for 30 FPS
  delay(30);


//  FastLED.delay(1000 / FRAMES_PER_SECOND);

  EVERY_N_MILLISECONDS( 20 ) {
    gHue++;  // slowly cycle the "base color" through the rainbow
  }
//  EVERY_N_SECONDS( 30 ) {
//    nextPattern();  // change patterns periodically
//  }

}


bool IR_idle() {
  return irrecv.decode(&ir_results) || ir_results.rawlen == 0;
}



#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
}


void processIR(int cmd) {
  switch (cmd) {
    case POWER_ON:
      addglitter = FALSE;

      power_state = 1;
      Serial.println("Power ON!");
      break;

    case POWER_OFF:
      power_state = 0;
      Serial.println("Power OFF");
      break;

    case BRIGHT_DOWN:
      brightness = max(brightness - 10, 0)  ;
      Serial.println("Brightness down");
      break;

    case BRIGHT_UP:
      brightness = min (brightness +10, 255) ;
      Serial.println("Brighness up");
      break;

    case WHITE:
      solid_color = TRUE;
      base_color = WHITE_C;
      Serial.println("Color WHITE");
      break;

    case RED_0:
      solid_color = TRUE;
      base_color = RED_0_C;
      Serial.println("Color Red 0");
      break;

    case RED_1:
      solid_color = TRUE;
      base_color = RED_1_C;
      Serial.println("Color Red 1");
      break;

    case RED_2:
      solid_color = TRUE;
      base_color = RED_2_C;
      Serial.println("Color Red 2");
      break;

    case RED_3:
      solid_color = TRUE;
      base_color = RED_3_C;
      Serial.println("Color Red 3");
      break;

    case RED_4:
      solid_color = TRUE;
      base_color = RED_4_C;
      Serial.println("Color RED 4");
      break;

    case GREEN_0:
      solid_color = TRUE;
      base_color = GREEN_0_C;
      Serial.println("Color GREEN 0");
      break;

    case GREEN_1:
      solid_color = TRUE;
      base_color = GREEN_1_C;
      Serial.println("Color GREEN 1");
      break;

    case GREEN_2:
      solid_color = TRUE;
      base_color = GREEN_2_C;
      Serial.println("Color GREEN 2");
      break;

    case GREEN_3:
      solid_color = TRUE;
      base_color = GREEN_3_C;
      Serial.println("Color GREEN 3");
      break;

    case GREEN_4:
      solid_color = TRUE;
      base_color = GREEN_4_C;
      Serial.println("Color GREEN 4");
      break;

    case BLUE_0:
      solid_color = TRUE;
      base_color = BLUE_0_C;
      Serial.println("Color BLUE 0");
      break;

    case BLUE_1:
      solid_color = TRUE;
      base_color = BLUE_1_C;
      Serial.println("Color BLUE 1");
      break;

    case BLUE_2:
      solid_color = TRUE;
      base_color = BLUE_2_C;
      Serial.println("Color BLUE 2");
      break;

    case BLUE_3:
      solid_color = TRUE;
      base_color = BLUE_3_C;
      Serial.println("Color BLUE 3");
      break;

    case BLUE_4:
      solid_color = TRUE;
      base_color = BLUE_4_C;
      Serial.println("Color BLUE 4");
      break;

    case FLASH:
      solid_color = FALSE;
      nextPattern();
      Serial.println("FLASH");
      break;

    case  STROBE:
      addglitter = ! addglitter;
      Serial.println("Strobe");
      break;


      //    #define MUTE        0x0D
      //    #define AV_TV       0x0B

  } // end switch

} // end processIR


void rainbow()
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}

void rainbowWithGlitter()
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter)
{
  if ( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void confetti()
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 13, 0, NUM_LEDS - 1 );
  leds[pos] += CHSV( gHue, 255, 192);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 62;
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for ( int i = 0; i < NUM_LEDS; i++) { //9948
    leds[i] = ColorFromPalette(palette, gHue + (i * 2), beat - gHue + (i * 10));
  }
}

void juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for ( int i = 0; i < 8; i++) {
    leds[beatsin16( i + 7, 0, NUM_LEDS - 1 )] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

// Fire2012 by Mark Kriegsman, July 2012
// as part of "Five Elements" shown here: http://youtu.be/knWiGsmgycY
////
// This basic one-dimensional 'fire' simulation works roughly as follows:
// There's a underlying array of 'heat' cells, that model the temperature
// at each point along the line.  Every cycle through the simulation,
// four steps are performed:
//  1) All cells cool down a little bit, losing heat to the air
//  2) The heat from each cell drifts 'up' and diffuses a little
//  3) Sometimes randomly new 'sparks' of heat are added at the bottom
//  4) The heat from each cell is rendered as a color into the leds array
//     The heat-to-color mapping uses a black-body radiation approximation.
//
// Temperature is in arbitrary units from 0 (cold black) to 255 (white hot).
//
// This simulation scales it self a bit depending on NUM_LEDS; it should look
// "OK" on anywhere from 20 to 100 LEDs without too much tweaking.
//
// I recommend running this simulation at anywhere from 30-100 frames per second,
// meaning an interframe delay of about 10-35 milliseconds.
//
// Looks best on a high-density LED setup (60+ pixels/meter).
//
//
// There are two main parameters you can play with to control the look and
// feel of your fire: COOLING (used in step 1 above), and SPARKING (used
// in step 3 above).
//
// COOLING: How much does the air cool as it rises?
// Less cooling = taller flames.  More cooling = shorter flames.
// Default 50, suggested range 20-100
#define COOLING  55

// SPARKING: What chance (out of 255) is there that a new spark will be lit?
// Higher chance = more roaring fire.  Lower chance = more flickery fire.
// Default 120, suggested range 50-200.
#define SPARKING 120


void Fire2012()
{
// Array of temperature readings at each simulation cell
  static byte heat[NUM_LEDS];

  // Step 1.  Cool down every cell a little
    for( int i = 0; i < NUM_LEDS; i++) {
      heat[i] = qsub8( heat[i],  random8(0, ((COOLING * 10) / NUM_LEDS) + 2));
    }

    // Step 2.  Heat from each cell drifts 'up' and diffuses a little
    for( int k= NUM_LEDS - 1; k >= 2; k--) {
      heat[k] = (heat[k - 1] + heat[k - 2] + heat[k - 2] ) / 3;
    }

    // Step 3.  Randomly ignite new 'sparks' of heat near the bottom
    if( random8() < SPARKING ) {
      int y = random8(7);
      heat[y] = qadd8( heat[y], random8(160,255) );
    }

    // Step 4.  Map from heat cells to LED colors
    for( int j = 0; j < NUM_LEDS; j++) {
      CRGB color = HeatColor( heat[j]);
      int pixelnumber;
      if( gReverseDirection ) {
        pixelnumber = (NUM_LEDS-1) - j;
      } else {
        pixelnumber = j;
      }
      leds[pixelnumber] = color;
    }
}
